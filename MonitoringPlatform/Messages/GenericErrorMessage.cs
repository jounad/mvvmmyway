﻿using System;

namespace MonitoringPlatform.Messages
{
    public class GenericErrorMessage
    {
        public Exception Error { get; set; }
    }
}
