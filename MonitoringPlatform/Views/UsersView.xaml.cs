﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using MonitoringPlatform.Messages;

namespace MonitoringPlatform.Views
{
    public partial class UsersView
    {
        public UsersView()
        {
            InitializeComponent();

            Messenger.Default.Register<GenericErrorMessage>(this, ReactOnUserError);
        }

        private void ReactOnUserError(GenericErrorMessage error)
        {
            MessageBox.Show(error.Error.Message);
        }

        ~UsersView()
        {
            Messenger.Default.Unregister<GenericErrorMessage>(this, ReactOnUserError);
        }
    }

}
