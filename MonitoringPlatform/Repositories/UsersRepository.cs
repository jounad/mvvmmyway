﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using MonitoringPlatform.Models;

namespace MonitoringPlatform.Repositories
{
    public interface IUsersRepository
    {
        IList<UserModel> GetUsers();
    }
    public class UsersRepository : IUsersRepository
    {
        public IList<UserModel> GetUsers()
        {
            return new List<UserModel> { new UserModel { Name = "User 1" }, new UserModel { Name = "User 2" } };
            // to test the messenger
            //throw new ApplicationException("Error just for test");
        }
    }
}
