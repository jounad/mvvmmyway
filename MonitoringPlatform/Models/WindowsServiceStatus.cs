﻿namespace MonitoringPlatform.Models
{
    public enum WindowsServiceStatus
    {
        Intermediate,
        Paused,
        Running,
        Stopped
    }

}
